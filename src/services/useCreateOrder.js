import { useMutation, useQueryClient } from "@tanstack/react-query";
import toast from "react-hot-toast";
import { createEditOrder, postOrder } from "./apiOrders";

export default function useCreateOrder ()
{
  
    const queryClient = useQueryClient()

  const { mutate: createOrder, isLoading:isCreating } = useMutation({
    mutationFn: postOrder,
    onSuccess: () => {
      toast.success("Order successfully created");
      queryClient.invalidateQueries({
        queryKey: ["orders"],
      });
    },
    onError: (err) => toast.error(err.message),
  });

  return {
    createOrder,
    isCreating
  }
}
