import { useMutation, useQueryClient } from "@tanstack/react-query";
import toast from "react-hot-toast";
import { createEditOrder, postItem } from "./apiOrders";

export default function useCreateType ()
{
  
    const queryClient = useQueryClient()

  const { mutate: createType, isLoading:isCreating } = useMutation({
    mutationFn: postItem,
    onSuccess: () => {
      toast.success("Order successfully created");
      queryClient.invalidateQueries({
        queryKey: ["orders"],
      });
    },
    onError: (err) => toast.error(err.message),
  });

  return {
    createType,
    isCreating
  }
}
