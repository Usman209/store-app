import { useMutation, useQueryClient } from "@tanstack/react-query"
import toast from "react-hot-toast"
import {  deleteCabin as deleteOrderApi, deleteOrderItem } from "./apiCabins"


export default function useDeleteOrder() {
  
  const queryClient = useQueryClient()
  
  const { isLoading, mutate:deleteOrder } = useMutation( {
    mutationFn: deleteOrderItem,
    onSuccess: () =>
    {
      toast.success('Order successfully deleted')
      queryClient.invalidateQueries( {
        queryKey:['orders']
      })
    },
    onError: ( err ) =>
    {
      toast.error( err.message )
    } 
  } )
  
  return {
    isLoading,
    deleteOrder
  }
}


