import supabase, { supabaseUrl } from "./supabase";

export async function getCabins ()
{
  
  const { data, error } = await supabase.from( 'cabins' ).select( '*' )
  if ( error )
  {
    console.log( error );
    throw new Error('cabins could not be loaded ')
  }
  
  return data;
}

export async function createEditCabin(newCabin,id)
{
console.log('new :',newCabin,'id',id);
  const hasImagePath = newCabin?.image?.startsWith?.( supabaseUrl )
  
  const imageName = `${ Math.random() }-${ newCabin.image.name }`.replaceAll( "/", "" )
  const imagePath= hasImagePath ? newCabin?.image : `${supabaseUrl}/storage/v1/object/public/cabin-images/${imageName}`

  // https://bliyegjmnivcjxgenlvu.supabase.co/storage/v1/object/public/cabin-images/cabin-001.jpg

  let query= supabase.from( 'cabins' )
  // 1. create cabin 
  if ( !id )
  {
    query=query.insert( [ { ...newCabin, image: imagePath } ] )
  }

  if ( id )
  {
  query=query.update({ ...newCabin, image: imagePath }).eq('id',id)
}

  const {data , error }= await query.select().single()
    if ( error )
    {
      console.log( error );
      throw new Error( 'cabins could not be created ' )
    }
  
  if ( hasImagePath ) return data;

  const {error:storageError} = await supabase.storage.from('cabin-images').upload(imageName,newCabin.image)
  
  if ( storageError )
  {
    await supabase.from( 'cabins' ).delete().eq( 'id', data.id )
    console.error( storageError );
}

  return data;
}


export async function deleteCabin (id)
{
  
  const { data, error } = await supabase.from( 'orders' ).delete().eq('id',id)
  if ( error )
  {
    console.log( error );
    throw new Error('order could not be deleted    ')
  }
  
  return data;
}

export async function deleteOrderItem( itemId ) {
  try {
    const response = await fetch( `http://localhost:4000/api/orders/${itemId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    } );
    if ( !response.ok ) {
      throw new Error( 'Failed to delete item' );
    }
    const data = await response.json();
    console.log( 'Response from deleteItem:', data );
    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Item could not be deleted' );
  }
}

export async function deleteItem( itemId ) {
  try {
    const response = await fetch( `http://localhost:4000/api/products/${itemId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    } );
    if ( !response.ok ) {
      throw new Error( 'Failed to delete item 123' );
    }
    const data = await response.json();
    console.log( 'Response from deleteItem 1234:', data );
    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Item could not be deleted 1221' );
  }
}



