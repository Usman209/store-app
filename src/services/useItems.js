import { useQuery } from "@tanstack/react-query";
import { getItems, getOrders } from "./apiOrders";

export default  function useItems() {
  const {
    isLoading,
    data: orders,
    error,
  } = useQuery({
    queryKey: ["orders"],
    queryFn: getItems,
  } );

  return {
    isLoading,
    error,
    orders
  }

}
