import { useMutation, useQueryClient } from "@tanstack/react-query"
import toast from "react-hot-toast"
import { deleteCabin as deleteCabinApi, deleteItem } from "./apiCabins"


export default function useDeleteType() {
  
  const queryClient = useQueryClient()
  
  const { isLoading, mutate:deleteCabin } = useMutation( {
    mutationFn: deleteItem,
    onSuccess: () =>
    {
      toast.success('Type successfully deleted')
      queryClient.invalidateQueries( {
        queryKey:['cabins']
      })
    },
    onError:(err)=>toast.error(err.message),
  } )
  
  return {isLoading,deleteCabin}
}


