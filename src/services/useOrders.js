import { useQuery } from "@tanstack/react-query";
import { getOrderItems, getOrders } from "./apiOrders";

export default  function useOrders() {
  const {
    isLoading,
    data: orders,
    error,
  } = useQuery({
    queryKey: ["orders"],
    queryFn: getOrderItems,
  } );

  return {
    isLoading,
    error,
    orders
  }

}
