import supabase, { supabaseUrl } from "./supabase";

export async function getOrders ()
{
  
 
  const { data, error } = await supabase.from( 'orders' ).select( '*' )
  if ( error )
  {
    console.log( error );
    throw new Error('orders could not be loaded ')
  } 
  return data;
}
export async function createEditOrder(newOrder,id)
{
console.log('new :',newOrder,'id',id);
  
  let query= supabase.from( 'orders' )
  // 1. create cabin 
  if ( !id )
  {
    query=query.insert( [ { ...newOrder } ] )
  }

  if ( id )
  {
  query=query.update({ ...newOrder }).eq('id',id)
  }
   
  const {
    data,
    error
  } = await query.select().maybeSingle()
    if ( error )
    {
      console.log('from here . error ', error );
      throw new Error( 'orders could not be created ' )
    }

  return data;
}


export async function deleteOrder (id)
{
  
  const { data, error } = await supabase.from( 'orders' ).delete().eq('id',id)
  if ( error )
  {
    console.log( error );
    throw new Error('orders could not be deleted    ')
  }
  
  return data;
}

// local file : fetch from local server 


export async function getItems() {
  try {
    const response = await fetch( 'http://localhost:4000/api/products' );
    if ( !response.ok ) {
      throw new Error( 'Failed to fetch orders' );
    }
    const data = await response.json();

    console.log( 'frmo here resp=======', data );

    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Orders could not be loaded' );
  }
}

export async function postItem( itemData ) {
  try {
    const response = await fetch( 'http://localhost:4000/api/products', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify( itemData ),
    } );
    if ( !response.ok ) {
      throw new Error( 'Failed to post item' );
    }
    const data = await response.json();
    console.log( 'Response from postItem:', data );
    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Item could not be posted' );
  }
}

export async function editItem( itemData, itemId )
{
  console.log( 'id========', itemId );
    console.log( 'itemData========', itemData );


  try {
    const response = await fetch( `http://localhost:4000/api/products/${itemId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify( itemData ),
    } );
    if ( !response.ok ) {
      throw new Error( 'Failed to edit item' );
    }
    const data = await response.json();
    console.log( 'Response from editItem:', data );
    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Item could not be edited' );
  }
}

export async function postOrder( itemData ) {
  try {
    const response = await fetch( 'http://localhost:4000/api/orders', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify( itemData ),
    } );
    if ( !response.ok ) {
      throw new Error( 'Failed to post order' );
    }
    const data = await response.json();
    console.log( 'Response from postItem:', data );
    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Item could not be posted' );
  }
}

export async function editOrderItem( itemData, itemId ) {
  console.log( 'id========', itemId );
  console.log( 'itemData========', itemData );


  try {
    const response = await fetch( `http://localhost:4000/api/orders/${itemId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify( itemData ),
    } );
    if ( !response.ok ) {
      throw new Error( 'Failed 123 to edit item' );
    }
    const data = await response.json();
    console.log( 'Response 124 from editItem:', data );
    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Item could not be edited' );
  }
}

export async function getOrderItems() {
  try {
    const response = await fetch( 'http://localhost:4000/api/orders' );
    if ( !response.ok ) {
      throw new Error( 'Failed to fetch orders' );
    }
    const data = await response.json();

    console.log( 'frmo here resp=======', data );

    return data;
  } catch ( error ) {
    console.error( error );
    throw new Error( 'Orders could not be loaded' );
  }
}


