import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createEditCabin } from "./apiCabins";
import toast from "react-hot-toast";
import { createEditOrder, editItem } from "./apiOrders";


export default function useEditOrder ()
{
  
   const queryClient = useQueryClient()


  const { mutate: editOrder, isLoading: isEditing } = useMutation( {
      mutationFn: ( {
        newOrderData,
        id
      } ) => editItem( newOrderData, id ),
      onSuccess: (data) => {
        toast.success("Order successfully edited");
        queryClient.invalidateQueries({
          queryKey: [ "orders" ],
        } );
        console.log( "Edit success data: ", data ); // Log success data here

      },
    onError: ( err ) =>
    {

     toast.error( err.message );
     console.error( "Edit error: ", err ); // Log error here
     },
    });

  return {
    editOrder,
    isEditing
  }
  
}




  
   