import { useEffect, useState } from "react";
import Heading from "../ui/Heading";
import Row from "../ui/Row";
import Button from "../ui/Button";
import OrderTable from "../features/orders/OrderTable";
import AddOrder from "../features/orders/AddOrder";

function Orders() {

  return (
    <>
      <Row type="horizontal">
        
        <Heading as="h1">All Orders</Heading>
        <p>Filter / Sort</p>
      </Row>
      <Row>
        <OrderTable />
        <AddOrder />
      </Row>
    </>
  );
}

export default Orders;
