import { useEffect, useState } from "react";
import Heading from "../ui/Heading";
import Row from "../ui/Row";
import Button from "../ui/Button";
import { getCabins } from "../services/apiCabins";
import RateTable from "../features/rates/RateTable";
import CreateRateForm from "../features/rates/CreateRateForm";
import AddRate from "../features/rates/AddRate";

function Rates() {

  return (
    <>
      <Row type="horizontal">
        <Heading as="h1">All Rates</Heading>
        {/* <p>Filter / Sort</p> */}
      </Row>
      <Row>
        <RateTable />
        <AddRate />
      </Row>
    </>
  );
}

export default Rates;
