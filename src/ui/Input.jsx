import styled from "styled-components";

const Input = styled.input`
  border: 1px solid var(--color-grey-300);
  background-color: var(--color-grey-0);
  border-radius: var(--border-radius-sm);
  padding: 0.8rem 0.1rem;  
  box-shadow: var(--shadow-sm);
`;

const Select = styled.select`
  border: 1px solid var(--color-grey-300);
  background-color: var(--color-grey-0);
  border-radius: var(--border-radius-sm);
  padding: 0.8rem 2.6rem; /* Match the padding with the Input component */
  box-shadow: var(--shadow-sm);
`;



export default Input;
export { Select };
