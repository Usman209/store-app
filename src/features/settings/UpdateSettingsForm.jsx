import Form from '../../ui/Form';
import FormRow from '../../ui/FormRow';
import Spinner from "../../ui/Spinner";
import Input from '../../ui/Input';
import useSettings from './useSettings';
import useUpdateSettings from './useUpdateSettings';

function UpdateSettingsForm ()
{
  const {
    isLoading,
    settings: {
      minBookingLength,
      maxBookingLength,
      maxGuestsPerBooking,
      breakfastPrice,
      flour,
      flourSafai,
      floorKhootPrice,
      flourKhootQunatity,
      gram,
      gramSafai,
      gramKhootPrice,
      gramKhootQunatity,
      turmericOrChilli,
      turmericOrChilliSafai,
      turmericOrChilliKhootPrice,
      turmericOrChilliKhootQunatity,
    } = {},
  } = useSettings();
  

  const { isUpdating, updateSetting } = useUpdateSettings()
  

  function handleUpdate ( e ,field)
  {
    const { value } = e.target;
    if (!value) return;
 
    updateSetting({ [field]: value });
  }
  
  if(isLoading) return <Spinner/>

  return (
    <Form>
      {/* <FormRow label="Minimum nights/booking">
        <Input
          type="number"
          id="min-nights"
          disabled={isUpdating}
          defaultValue={minBookingLength}
          onBlur={(e) => handleUpdate(e, "minBookingLength")}
        />
      </FormRow>
      <FormRow label="Maximum nights/booking">
        <Input
          type="number"
          id="max-nights"
          defaultValue={maxBookingLength}
          disabled={isUpdating}
          onBlur={(e) => handleUpdate(e, "maxBookingLength")}
        />
      </FormRow>
      <FormRow label="Maximum guests/booking">
        <Input
          type="number"
          id="max-guests"
          defaultValue={maxGuestsPerBooking}
          disabled={isUpdating}
          onBlur={(e) => handleUpdate(e, "maxGuestsPerBooking")}
        />
      </FormRow>
      <FormRow label="Breakfast price">
        <Input
          type="number"
          id="breakfast-price"
          defaultValue={breakfastPrice}
          disabled={isUpdating}
          onBlur={(e) => handleUpdate(e, "breakfastPrice")}
        />
      </FormRow> */}

      <FormRow label="Flour Price">
        <Input
          type="number"
          id="flour-price"
          disabled={isUpdating}
          defaultValue={flour}
          onBlur={(e) => handleUpdate(e, "flour")}
        />
      </FormRow>
      <FormRow label="Flour Khoot Price">
        <Input
          type="number"
          id="flour-khoot"
          disabled={isUpdating}
          defaultValue={floorKhootPrice}
          onBlur={(e) => handleUpdate(e, "floorKhootPrice")}
        />
      </FormRow>

      <FormRow label="Flour Khoot Quantity ">
        <Input
          type="number"
          id="flour-khoot-quantity"
          disabled={isUpdating}
          defaultValue={flourKhootQunatity}
          onBlur={(e) => handleUpdate(e, "flourKhootQunatity")}
        />
      </FormRow>

      <FormRow label="Flour Safai Price">
        <Input
          type="number"
          id="flour-safai"
          disabled={isUpdating}
          defaultValue={flourSafai}
          onBlur={(e) => handleUpdate(e, "flourSafai")}
        />
      </FormRow>

      {/* // gram */}

      <FormRow label="Gram Price">
        <Input
          type="number"
          id="gram-price"
          disabled={isUpdating}
          defaultValue={gram}
          onBlur={(e) => handleUpdate(e, "gram")}
        />
      </FormRow>

      <FormRow label="Gram Khoot Price">
        <Input
          type="number"
          id="gram-khoot"
          disabled={isUpdating}
          defaultValue={gramKhootPrice}
          onBlur={(e) => handleUpdate(e, "gramKhootPrice")}
        />
      </FormRow>

      <FormRow label="Gram Khoot Quantity">
        <Input
          type="number"
          id="gram-khoot-quantity"
          disabled={isUpdating}
          defaultValue={gramKhootQunatity}
          onBlur={(e) => handleUpdate(e, "gramKhootQunatity")}
        />
      </FormRow>

      <FormRow label="Gram Safai Price">
        <Input
          type="number"
          id="gram-safai"
          disabled={isUpdating}
          defaultValue={gramSafai}
          onBlur={(e) => handleUpdate(e, "gramSafai")}
        />
      </FormRow>

      {/* chilli */}

      <FormRow label="Turmeric/Chilli Price ">
        <Input
          type="number"
          id="turmeric-chilli-price"
          disabled={isUpdating}
          defaultValue={turmericOrChilli}
          onBlur={(e) => handleUpdate(e, "turmericOrChilli")}
        />
      </FormRow>

      <FormRow label="Turmeric/Chilli Khoot Price">
        <Input
          type="number"
          id="turmeric-chilli-khoot"
          disabled={isUpdating}
          defaultValue={turmericOrChilliKhootPrice}
          onBlur={(e) => handleUpdate(e, "turmericOrChilliKhootPrice")}
        />
      </FormRow>

      <FormRow label="Turmeric/Chilli Khoot Quantity ">
        <Input
          type="number"
          id="turmeric-chilli-khoot-quantity"
          disabled={isUpdating}
          defaultValue={turmericOrChilliKhootQunatity}
          onBlur={(e) => handleUpdate(e, "turmericOrChilliKhootQunatity")}
        />
      </FormRow>

      <FormRow label="Turmeric/Chilli Safai Price">
        <Input
          type="number"
          id="turmeric-chilli-safai"
          disabled={isUpdating}
          defaultValue={turmericOrChilliSafai}
          onBlur={(e) => handleUpdate(e, "turmericOrChilliSafai")}
        />
      </FormRow>
    </Form>
  );
}

export default UpdateSettingsForm;
