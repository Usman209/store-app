import styled from "styled-components";
import { formatCurrency } from "../../utils/helpers";
import CreateOrderForm from "./CreateOrderForm";
import useDeleteOrder from "../../services/useDeleteOrder";
import { HiPencil, HiSquare2Stack, HiTrash } from "react-icons/hi2";
import useCreateOrder from "../../services/useCreateOrder";
import Modal from "../../ui/Modal";
import ConfirmDelete from "../../ui/ConfirmDelete";
import Table from "../../ui/Table";
import Menus from "../../ui/Menus";
import { useRef } from "react";
import ReactToPrint from "react-to-print";

import { useState, useEffect } from "react";

const TableRow = styled.div`
  display: grid;
  grid-template-columns: 0.6fr 1.8fr 2.2fr 1fr 1fr 1fr;
  column-gap: 2.4rem;
  align-items: center;
  padding: 1.4rem 2.4rem;

  &:not(:last-child) {
    border-bottom: 1px solid var(--color-grey-100);
  }
`;

const Img = styled.img`
  display: block;
  width: 6.4rem;
  aspect-ratio: 3 / 2;
  object-fit: cover;
  object-position: center;
  transform: scale(1.5) translateX(-7px);
`;

const Order = styled.div`
  font-size: 1.6rem;
  font-weight: 600;
  color: var(--color-grey-600);
  font-family: "Sono";
`;

const Price = styled.div`
  font-family: "Sono";
  font-weight: 600;
`;

const Discount = styled.div`
  font-family: "Sono";
  font-weight: 500;
  color: var(--color-green-700);
`;

const PrintContent = ({
  orderId,
  customerName,
  customerMobile,
  weight,
  price,
  comment,
}) => {
  const [svgContent, setSVGContent] = useState(null);

  useEffect(() => {
    const fetchSVGContent = async () => {
      try {
        const response = await fetch(
          `http://localhost:4000/api/orders/${orderId}`
        );
        const data = await response.json();
        setSVGContent(data.svg);
      } catch (error) {
        console.error("Error fetching SVG content:", error);
      }
    };

    fetchSVGContent();
  }, [orderId]);

  return (
    <div
      style={{
        border: "1px solid #ccc",
        padding: "25px",
        // borderRadius: "5px",
        // boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
        backgroundColor: "#fff",
        fontFamily: "Arial, sans-serif",
        // width: "300px",
        margin: "20px",
      }}
    >
      {svgContent && <div dangerouslySetInnerHTML={{ __html: svgContent }} />}
    </div>
  );
};

const fetchSVGContent = async (orderId) => {
  // Example URL to fetch SVG content for a specific order ID
  const response = await fetch(`http://localhost:4000/api/orders/${orderId}`);
  const data = await response.json();

  return data.svg;
};

export default function OrderRow({ order }) {
  const componentRef = useRef();

  // const { isLoading, deleteOrder } = useDeleteOrder();
  const { isLoading, deleteOrder } = useDeleteOrder();

  const { isCreating, createOrder } = useCreateOrder();
  
  const {
    _id: orderId,
    created_at,
    orderNumber,
    type,
    isClean,
    isDeduction,
    customerName,
    customerMobile,
    weight,
    rate,
    balanceItem,
    discount,
    price,
    advance,
    comment,
  } = order;

  return (
    <Table.Row>
      <Order>{orderNumber}</Order>
      <Order>{customerName}</Order>
      <Order>{customerMobile}</Order>
      <Order>{weight}</Order>
      <Order>{price}</Order>

      {/* <Discount>{formatCurrency(discount)}</Discount> */}
      <div>
        <div>
          <ReactToPrint
            trigger={() => (
              <button
                onClick={() => window.print()}
                style={{
                  backgroundColor: "#007bff",
                  color: "#fff",
                  border: "none",
                  borderRadius: "5px",
                  padding: "10px",
                  cursor: "pointer",
                }}
              >
                Print !
              </button>
            )}
            content={() => componentRef.current}
          />

          {/* Print content */}
          <div style={{ display: "none" }}>
            <div ref={componentRef}>
              <PrintContent
                orderId={orderId}
                customerName={customerName}
                customerMobile={customerMobile}
                weight={weight}
                price={price}
                comment={comment}
              />
            </div>
          </div>
        </div>

        <Modal>
          <Menus.Menu>
            <Menus.Toggle id={orderId} />

            <Menus.List id={orderId}>
              {/* <Menus.Button icon={<HiSquare2Stack />} onClick={handleDuplicate}>
                Duplicate
              </Menus.Button> */}

              <Modal.Open opens="edit">
                <Menus.Button icon={<HiPencil />}>Edit</Menus.Button>
              </Modal.Open>

              <Modal.Open opens="delete">
                <Menus.Button icon={<HiTrash />}>Delete</Menus.Button>
              </Modal.Open>
            </Menus.List>
          </Menus.Menu>

          <Modal.Window name="edit">
            <CreateOrderForm cabintoEdit={order} />
          </Modal.Window>

          <Modal.Window name="delete">
            <ConfirmDelete
              resourceName="cabin"
              disabled={isLoading}
              onConfirm={() => deleteOrder(orderId)}
            />
          </Modal.Window>
        </Modal>
      </div>
    </Table.Row>
  );
}
