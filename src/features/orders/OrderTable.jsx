import React, { useState } from "react";
import Table from "../../ui/Table";
import Spinner from "../../ui/Spinner";
import OrderRow from "./OrderRow";
import useOrders from "../../services/useOrders";
import Menus from "../../ui/Menus";
import styles from "./SearchBar.module.css";

function OrderTable() {
  const [value, setValue] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(4); // Number of items per page
  const { isLoading, error, orders } = useOrders();

  if (error) return <div>Error: {error}</div>;
  if (isLoading) return <Spinner />;

  // Filter orders based on the search value
const filteredOrders = orders.filter(
  (order) =>
    order?.customerName?.toLowerCase().includes(value.toLowerCase()) ||
    order?.customerMobile?.toLowerCase().includes(value.toLowerCase()) ||
    order?.orderNumber?.toString().includes(value)
);



  // Calculate pagination values
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentOrders = filteredOrders.slice(indexOfFirstItem, indexOfLastItem);

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <Menus>
      <div className={styles.container}>
        <input
          type="text"
          className={styles.textbox}
          placeholder="Search data..."
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      </div>
      <Table columns="0.6fr 1.8fr 2.2fr 1fr 1fr 1fr">
        <Table.Header>
          <div>Order #</div>
          <div>Customer Name</div>
          <div>Customer Mobile</div>
          <div>Weight</div>
          <div>Price</div>
          <div></div>
        </Table.Header>
        <Table.Body
          data={currentOrders} // Render current page orders
          render={(order) => <OrderRow order={order} key={order._id} />}
        />
      </Table>
      {/* Pagination */}
      <div
        className="pagination"
        style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}
      >
        {Array.from({
          length: Math.ceil(filteredOrders.length / itemsPerPage),
        }).map((_, index) => (
          <button
            key={index}
            onClick={() => paginate(index + 1)}
            style={{
              backgroundColor: "#007bff",
              color: "white",
              border: "none",
              padding: "10px 15px",
              margin: "0 5px",
              cursor: "pointer",
              borderRadius: "5px",
            }}
          >
            {index + 1}
          </button>
        ))}
      </div>
    </Menus>
  );
}

export default OrderTable;
