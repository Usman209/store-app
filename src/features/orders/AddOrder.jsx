import { useState } from "react";
import Button from "../../ui/Button";
import CreateOrderForm from "./CreateOrderForm";
import Modal from "../../ui/Modal";

export default function AddOrder() {
  return (
    <Modal>
      <Modal.Open opens="cabin-form">
        <Button>Add new Order</Button>
      </Modal.Open>
      <Modal.Window name="cabin-form">
        <CreateOrderForm />
      </Modal.Window>

      {/* <Modal.Open opens="table">
        <Button>show table</Button>
      </Modal.Open> */}
      <Modal.Window name="table">
        <CreateOrderForm />
      </Modal.Window>
    </Modal>
  );
}

// export default function AddOrder ()
// {
//     const [isOpenModel, setIsOpenModel] = useState(false);

//   return (
//     <div>
//       <Button onClick={() => setIsOpenModel((show) => !show)}>
//         add new cabin
//       </Button>
//       { isOpenModel && <Modal onClick={()=>setIsOpenModel(false)}>
//         <CreateOrderForm onCloseMode={ () => setIsOpenModel( false ) } />
//       </Modal> }
//     </div>
//   );
// }
