import styled from "styled-components";

import Input, { Select } from "../../ui/Input";
import Form from "../../ui/Form";
import Button from "../../ui/Button";
import FileInput from "../../ui/FileInput";
import Textarea from "../../ui/Textarea";
import { useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createEditOrder } from "../../services/apiOrders";
import toast from "react-hot-toast";
import FormRow from "../../ui/FormRow";
import useCreateOrder from "../../services/useCreateOrder";
import useEditOrder from "../../services/useEditOrder";
import { useEffect, useState, useRef } from "react";
import useSettings from "../settings/useSettings";

const Label = styled.label`
  font-weight: 500;
`;

const Error = styled.span`
  font-size: 1.4rem;
  color: var(--color-red-700);
`;

const StyledFormRow = styled.div`
  display: flex;
  align-items: center;
  grid-template-columns: repeat(2, auto); /* Adjust the number of columns */
  gap: 3rem; /* Adjust the gap between elements */
  /* Additional styles for your grid */
`;

const StyledTextLine = styled.span`
  color: #333333 100%;
  font-size: 17px;
  font-family: NeuropoliticalRg-Regular;
  word-spacing: 0px;
`;

function CreateOrderForm({ cabintoEdit = {}, onCloseModal }) {
  const [formData, setFormData] = useState({
    type: "flour", // Default type
    isCleanWithDeduction: false,
    isCleanWithoutDeduction: false,
    isGrindWithDeduction: false,
    isGrindWithoutDeduction: false,
    netWeight: "",
    finalPrice: 0,
    balance: 0, // New state for balance
    editablePrice: 0, // Initialize editablePrice
    selectedItemId: "", // Initially set to an empty string
  });

  const {
    type,
    isCleanWithDeduction,
    isCleanWithoutDeduction,
    isGrindWithDeduction,
    isGrindWithoutDeduction,
    netWeight,
    finalPrice,
  } = formData;

  const [advanceItem, setAdvanceItem] = useState(0);
  const [advancePay, setAdvancePay] = useState(0);

  const [initialWeight, setInitialWeight] = useState(0);

  const { _id: editId, ...editValues } = cabintoEdit;

  const [rate, setRate] = useState(0);
  const [balanceItem, setBalanceItem] = useState(0);

  const [items, setItems] = useState([]);
  const [selectedItem, setSelectedItem] = useState();
  const [item, setItem] = useState();
  const prevSelectedItemRef = useRef();

  const isEditSession = Boolean(editId);

  const {
    register,
    handleSubmit,
    reset,
    getValues,
    setValue,
    formState,
    watch, // Include watch from react-hook-form
  } = useForm({
    defaultValues: isEditSession ? editValues : {},
  });
  const { errors } = formState;

  // const { isCreating, createOrder } = useCreateOrder();
  const { isCreating, createOrder } = useCreateOrder();
  // const { isEditing, editOrder } = useEditOrder();
  const { isEditing, editOrder } = useEditOrder();

  const watchedWeight = watch("weight");
  const watchedAdvance = watch("advanceItem");

  const isWorking = isCreating || isEditing;

  useEffect(() => {
    // Check if the previous selectedItem is different from the current one
    if (prevSelectedItemRef.current !== selectedItem) {
      // Reset the form field with the ID "weight"
      setValue("weight", ""); // Assuming you're using react-hook-form's setValue method
    }

    // Update the ref with the current selectedItem
    prevSelectedItemRef.current = selectedItem;
  }, [selectedItem, setValue]);

useEffect(() => {
  // Update formData state based on cabintoEdit data when it's provided
  if (cabintoEdit._id) {
    setAdvanceItem(cabintoEdit.advanceItem);
    setAdvancePay(cabintoEdit.advancePay);

    // Update fields in formData state
    setFormData((prevData) => ({
      ...prevData,
      type: cabintoEdit.type,
      selectedItemId: cabintoEdit.type, // Update selectedItemId as well
      isCleanWithDeduction: cabintoEdit.isCleanWithDeduction, // Update isCleanWithDeduction
      isCleanWithoutDeduction: cabintoEdit.isCleanWithoutDeduction, // Update isCleanWithoutDeduction
      isGrindWithDeduction: cabintoEdit.isGrindWithDeduction, // Update isGrindWithDeduction
      isGrindWithoutDeduction: cabintoEdit.isGrindWithoutDeduction, // Update isGrindWithoutDeduction
    }));
  }
}, [cabintoEdit]);


  useEffect(() => {
    // Define the cleaning and grinding functions
    const cleaning = (
      isCleanWithDeduction,
      isCleanWithoutDeduction,
      weight
    ) => {
      if (isCleanWithoutDeduction) {
        return item?.priceCleaning * weight;
      } else if (isCleanWithDeduction) {
        const weightSubtract = weight * item?.priceCleaningWithDeductionFactor;
        const res = weight - weightSubtract;
        return res * item?.priceCleaningWithDeduction;
      }
      return 0;
    };

    const grinding = (
      isGrindWithDeduction,
      isGrindWithoutDeduction,
      weight
    ) => {
      if (isGrindWithDeduction) {
        const deduction = item?.priceGridingWithDeductionFactor * weight;
        const newWeight = weight - deduction; // Update weight after deduction
        return newWeight * item?.priceGridingWithDeduction;
      } else if (isGrindWithoutDeduction) {
        return item?.priceGriding * weight;
      }
      return 0;
    };

    const fetchItems = async () => {
      try {
        const response = await fetch("http://localhost:4000/api/products");
        if (!response.ok) {
          throw new Error("Failed to fetch items");
        }
        const data = await response.json();
        setItems(data);
        setItem(data[0]);
      } catch (error) {
        console.error(error);
      }
    };

    if (!selectedItem) {
      fetchItems();
    }
    const fetchItem = async (selectedItem) => {
      try {
        const response = await fetch(
          `http://localhost:4000/api/products/type/${selectedItem}`
        );
        if (!response.ok) {
          throw new Error("Failed to fetch items");
        }
        const data = await response.json();
        setItem(data[0]);

        // setSelectedItem;
      } catch (error) {
        console.error(error);
      }
    };

    if (selectedItem) {
      // Fetch item only if selectedItem is present and has changed
      fetchItem(selectedItem);
    }

    prevSelectedItemRef.current = selectedItem;

    // Check if the previous selectedItem is different from the current one
    if (prevSelectedItemRef.current !== selectedItem) {
      // Reset the form field with the ID "weight"
      setValue("weight", ""); // Assuming you're using react-hook-form's setValue method
    }

    // Calculate the prices for cleaning and grinding
    const cleaningPrice = cleaning(
      isCleanWithDeduction,
      isCleanWithoutDeduction,
      watchedWeight
    );
    const grindingPrice = grinding(
      isGrindWithDeduction,
      isGrindWithoutDeduction,
      watchedWeight
    );
    const rate = cleaningPrice + grindingPrice;

    // Calculate the new balance
    let newWeight = watchedWeight;
    if (isGrindWithDeduction) {
      const deduction = Math.round(0.025 * newWeight); // Round the deduction to the nearest integer
      newWeight -= deduction; // Update weight after deduction
    }
    if (isCleanWithDeduction) {
      const deduction = Math.round(0.025 * newWeight); // Round the deduction to the nearest integer

      newWeight -= deduction; // Update weight after deduction
    } else if (isCleanWithoutDeduction) {
      const deduction = 0.0; // No deduction
      newWeight -= deduction; // Update weight
    }

    const newBalance = newWeight - advanceItem;

    // Set the form data with updated values
    setFormData((prevData) => ({
      ...prevData,
      finalPrice: rate,
      editablePrice: rate, // Initialize editablePrice with the calculated value
      balance: newBalance,
    }));
    // Update the initial weight if it's not set
    if (!initialWeight) {
      setInitialWeight(watchedWeight);
    }
    return () => {}; // Reset all fields here
    // For example:
    // setFormData((prevData) => ({
    //   ...prevData,
    //   fieldName: defaultValue,
    // }));
  }, [
    isCleanWithDeduction,
    isCleanWithoutDeduction,
    isGrindWithDeduction,
    isGrindWithoutDeduction,
    watchedWeight,
    advanceItem,
    selectedItem,
    advancePay,
    formData.editablePrice,
  ]);

  function onSubmit(data) {
    data.type = formData.selectedItemId ? formData.selectedItemId : item.name;

    // data.price = finalPrice;
    data.price = Math.round(formData.editablePrice - advancePay);
    // Rest of your logic here
    data.advancePay = advancePay;
    data.isCleanWithDeduction = formData.isCleanWithDeduction;
    data.isCleanWithoutDeduction = formData.isCleanWithoutDeduction;
    data.isGrindWithDeduction = formData.isGrindWithDeduction;
    data.isGrindWithoutDeduction = formData.isGrindWithoutDeduction;
    data.advanceItem = advanceItem;
    data.netWeight = formData?.balance;

    if (isEditSession) {
      editOrder(
        {
          newOrderData: { ...data }, // Pass an empty string for the image
          id: editId, // Your ID value here
        },
        {
          onSuccess: (data) => {
            reset();
            onCloseModal?.();
          },
        }
      );
    } else
      createOrder(
        { ...data },
        {
          onSuccess: (data) => {
            reset();
            onCloseModal?.();
          },
        }
      );
  }

  function onError(error) {
    console.log(error);
  }

  const formatDate = (date) => {
    const options = { month: "numeric", day: "numeric", year: "numeric" };
    return date.toLocaleDateString(undefined, options);
  };

  // Function to format time as HH:MM:SS AM/PM
  const formatTime = (date) => {
    const options = {
      hour: "numeric",
      minute: "2-digit",
      second: "2-digit",
      hour12: true,
    };
    return date.toLocaleTimeString(undefined, options);
  };

  return (
    <Form
      onSubmit={handleSubmit(onSubmit, onError)}
      type={onCloseModal ? "modal" : "regular"}
    >
      <p>
        Date: {formatDate(new Date())}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Time: {formatTime(new Date())}
      </p>
      <br></br>

      <div style={{ display: "flex" }}>
        <div style={{ marginRight: "8px" }}>
          <FormRow label="">
            <Input
              type="text"
              id="customerName"
              customerName="customerName" // Ensure the name attribute matches the key in editValues
              placeholder="Name"
              disabled={isWorking}
              {...register("customerName", {
                required: "This field is required",
              })}
              style={{ padding: "8px" }}
            />
          </FormRow>
        </div>
        <div style={{ marginLeft: "-30px" }}>
          <FormRow label="">
            <Input
              type="text"
              id="customerMobile"
              placeholder="Mobile"
              disabled={isWorking}
              {...register("customerMobile", {
                required: "This field is required",
              })}
              style={{ padding: "8px" }}
            />
          </FormRow>
        </div>
      </div>
      <br></br>

      <FormRow label="Select Type of Item">
        <Select
          id="type"
          value={formData.selectedItemId}
          onChange={(e) => {
            const selectedItemId = e.target.value; // Get the selected item's name directly
            setSelectedItem(selectedItemId); // Set the selected item's name in the state
            setFormData({ ...formData, selectedItemId }); // Update the form data with the selected item's name
          }}
          disabled={isWorking}
          style={{ padding: "10px", paddingLeft: "20px", marginRight: "-25px" }}
        >
          {/* Map over the items array to create options for each item */}
          {items?.map((option) => (
            <option key={option._id} value={option.name}>
              {" "}
              {/* Use 'name' as value */}
              {option?.name}
            </option>
          ))}
        </Select>
      </FormRow>
      <FormRow label="Weight">
        <Input
          type="text" // Change type to text to accept decimal numbers
          id="weight"
          pattern="[0-9]+(\.[0-9]+)?" // Specify a pattern to allow decimal numbers
          inputMode="numeric" // Set input mode to numeric for mobile devices
          disabled={isWorking}
          {...register("weight", { required: "This field is required" })}
        />
      </FormRow>

      <FormRow
        label="Cleaning"
        style={{ display: "flex", flexDirection: "column" }}
      >
        <div style={{ display: "flex", flexDirection: "column", gap: "30px" }}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <input
              type="checkbox"
              id="isCleanWithDeduction"
              checked={isCleanWithDeduction}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  isCleanWithDeduction: e.target.checked,
                  isCleanWithoutDeduction: !e.target.checked
                    ? isCleanWithoutDeduction
                    : false,
                });
              }}
              disabled={isWorking}
              style={{ marginRight: "10px", transform: "scale(1.5)" }} // Increase size
            />
            <label
              htmlFor="isCleanWithDeduction"
              style={{ fontSize: "1.5rem" }}
            >
              {" "}
              {/* Increase font size */}
              Cleaning with Deduction
            </label>
          </div>
          <div style={{ display: "flex", alignItems: "center" }}>
            <input
              type="checkbox"
              id="clean-without-deduction"
              checked={isCleanWithoutDeduction}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  isCleanWithoutDeduction: e.target.checked,
                  isCleanWithDeduction: !e.target.checked
                    ? isCleanWithDeduction
                    : false,
                });
              }}
              disabled={isWorking}
              style={{ marginRight: "10px", transform: "scale(1.5)" }} // Increase size
            />
            <label
              htmlFor="clean-without-deduction"
              style={{ fontSize: "1.5rem" }}
            >
              {" "}
              {/* Increase font size */}
              Cleaning without Deduction
            </label>
          </div>
        </div>
      </FormRow>

      <FormRow
        label="Select Grind"
        style={{ display: "flex", flexDirection: "column" }}
      >
        <div style={{ display: "flex", flexDirection: "column", gap: "20px" }}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <input
              type="checkbox"
              id="grinding-with-deduction"
              checked={isGrindWithDeduction}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  isGrindWithDeduction: e.target.checked,
                  isGrindWithoutDeduction: !e.target.checked
                    ? isGrindWithoutDeduction
                    : false,
                });
              }}
              disabled={isWorking}
              style={{ marginRight: "10px", transform: "scale(1.5)" }} // Increase size
            />
            <label
              htmlFor="grinding-with-deduction"
              style={{ fontSize: "1.5rem" }}
            >
              Grinding with Deduction
            </label>
          </div>
          <div style={{ display: "flex", alignItems: "center" }}>
            <input
              type="checkbox"
              id="grinding-without-deduction"
              checked={isGrindWithoutDeduction}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  isGrindWithoutDeduction: e.target.checked,
                  isGrindWithDeduction: !e.target.checked
                    ? isGrindWithDeduction
                    : false,
                });
              }}
              disabled={isWorking}
              style={{ marginRight: "10px", transform: "scale(1.5)" }} // Increase size
            />
            <label
              htmlFor="grinding-without-deduction"
              style={{ fontSize: "1.5rem" }}
            >
              Grinding without Deduction
            </label>
          </div>
        </div>
      </FormRow>

      <FormRow label="Advance Atta">
        <Input
          type="number"
          id="advanceItem"
          disabled={isWorking}
          value={advanceItem}
          onChange={(e) => setAdvanceItem(parseFloat(e.target.value))}
        />
      </FormRow>

      <FormRow label="Net Weight">
        <Input
          type="text"
          id="balance"
          value={formData?.balance} // Display balance with 2 decimal places
          disabled
        />
      </FormRow>

      {/* <FormRow label="Rate">
        <Input
          type="number"
          id="rate"
          value={finalPrice}
          disabled={isWorking}
          readOnly
        />
      </FormRow> */}

      <FormRow>
        <Label>Advance Pay</Label>
        <Input
          type="number"
          id="advancePay"
          name="advancePay"
          value={advancePay}
          onChange={(e) => {
            const value = parseFloat(e.target.value);
            setAdvancePay(value >= 0 ? value : 0); // Ensure Advance Pay is not negative
          }}
          disabled={isCreating || isEditing}
        />
      </FormRow>

      <FormRow label="Total Price">
        <Input
          type="number"
          id="editablePrice"
          value={
            advancePay > 0
              ? (formData.editablePrice - advancePay).toFixed(2)
              : formData.editablePrice.toFixed(2) // If Advance Pay is zero, use the original Editable Price
          }
          onChange={(e) => {
            const newEditablePrice = parseFloat(e.target.value);
            setFormData((prevData) => ({
              ...prevData,
              editablePrice: newEditablePrice || 0,
            }));
          }}
        />
      </FormRow>

      <FormRow label="Comment" error={errors?.comment?.message}>
        <div style={{ width: "calc(100% - 2rem)" }}>
          <Textarea
            id="comment"
            disabled={isWorking}
            {...register("comment")} // Remove the "required" validation rule
            style={{ width: "100%", height: "100%" }} // Ensure the Textarea fills the div
          />
        </div>
      </FormRow>

      <FormRow>
        {/* type is an HTML attribute! */}
        <Button
          variation="secondary"
          type="reset"
          onClick={() => onCloseModal?.()}
        >
          Cancel
        </Button>
        <Button disabled={isWorking}>
          {isEditSession ? "Edit order" : "Add order"}
        </Button>
      </FormRow>
    </Form>
  );
}

export default CreateOrderForm;
