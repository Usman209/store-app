import { useState } from "react";
import Button from "../../ui/Button";
import CreateRateForm from "./CreateRateForm";
import Modal from "../../ui/Modal";

export default function AddRate() {
  return (
    <Modal>
      <Modal.Open opens="cabin-form">
        <Button>Add new Item</Button>
      </Modal.Open>
      <Modal.Window name="cabin-form">
        <CreateRateForm />
      </Modal.Window>

      {/* <Modal.Open opens="table">
        <Button>show table</Button>
      </Modal.Open> */}
      <Modal.Window name="table">
        <CreateRateForm />
      </Modal.Window>
    </Modal>
  );
}

// export default function AddRate ()
// {
//     const [isOpenModel, setIsOpenModel] = useState(false);

//   return (
//     <div>
//       <Button onClick={() => setIsOpenModel((show) => !show)}>
//         add new cabin
//       </Button>
//       { isOpenModel && <Modal onClick={()=>setIsOpenModel(false)}>
//         <CreateRateForm onCloseMode={ () => setIsOpenModel( false ) } />
//       </Modal> }
//     </div>
//   );
// }
