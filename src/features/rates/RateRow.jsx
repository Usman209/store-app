import styled from "styled-components";
import { formatCurrency } from "../../utils/helpers";
import CreateRateForm from "./CreateRateForm";
import useDeleteType from "../../services/useDeleteType";

import useDeleteOrder from "../../services/useDeleteOrder";

import { HiPencil, HiSquare2Stack, HiTrash } from "react-icons/hi2";
import useCreateType from "../../services/useCreateType";
import Modal from "../../ui/Modal";
import ConfirmDelete from "../../ui/ConfirmDelete";
import Table from "../../ui/Table";
import Menus from "../../ui/Menus";
import { useRef } from "react";
import ReactToPrint from "react-to-print";

const TableRow = styled.div`
  display: grid;
  grid-template-columns: 0.6fr 1.8fr 2.2fr 1fr 1fr 1fr;
  column-gap: 2.4rem;
  align-items: center;
  padding: 1.4rem 2.4rem;

  &:not(:last-child) {
    border-bottom: 1px solid var(--color-grey-100);
  }
`;

const Img = styled.img`
  display: block;
  width: 6.4rem;
  aspect-ratio: 3 / 2;
  object-fit: cover;
  object-position: center;
  transform: scale(1.5) translateX(-7px);
`;

const Rate = styled.div`
  font-size: 1.6rem;
  font-weight: 600;
  color: var(--color-grey-600);
  font-family: "Sono";
`;

const Price = styled.div`
  font-family: "Sono";
  font-weight: 600;
`;

const Discount = styled.div`
  font-family: "Sono";
  font-weight: 500;
  color: var(--color-green-700);
`;

export default function RateRow({ order }) {
  const componentRef = useRef();

  const { isLoading, deleteCabin } = useDeleteType();
  // const { isLoading, deleteOrder } = useDeleteOrder();

  const { isCreating, createRate } = useCreateType();

  const {
    id: orderId,
    name,
    priceCleaning,
    priceCleaningWithDeduction,
    priceCleaningWithDeductionFactor,
    priceGriding,
    priceGridingWithDeduction,
    priceGridingWithDeductionFactor,
  } = order;

  // const {
  //   id: orderId,
  //   customerName,
  //   maxCapacity,
  //   regularPrice,
  //   discount,
  //   image,
  //   description,
  // } = order;

  //   function handleDuplicate ()
  //   {
  //     createRate({
  //       customerName: `copy of ${customerName}`,
  //       maxCapacity,
  //       regularPrice,
  //       discount,
  //       image,
  //       description,
  //     });

  // }

  // <div>Token Number</div>
  //          <div>Customer Name</div>
  //          <div>Customer Mobile</div>
  //          <div>Type</div>
  //          <div>Weight</div>
  //          <div>Price</div>

  return (
    <Table.Row>
      <Rate>{name}</Rate>
      <Rate>{priceCleaning}</Rate>
      <Rate>{priceCleaningWithDeduction}</Rate>
      <Rate>{priceCleaningWithDeductionFactor}</Rate>
      <Rate>{priceGriding}</Rate>
      <Rate>{priceGridingWithDeduction}</Rate>
      <Rate>{priceGridingWithDeductionFactor}</Rate>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>

      <div>
        <Modal>
          <Menus.Menu>
            <Menus.Toggle id={orderId} />

            <Menus.List id={orderId}>
              {/* <Menus.Button icon={<HiSquare2Stack />} onClick={handleDuplicate}>
                Duplicate
              </Menus.Button> */}

              <Modal.Open opens="edit">
                <Menus.Button icon={<HiPencil />}>Edit</Menus.Button>
              </Modal.Open>

              <Modal.Open opens="delete">
                <Menus.Button icon={<HiTrash />}>Delete</Menus.Button>
              </Modal.Open>
            </Menus.List>
          </Menus.Menu>

          <Modal.Window name="edit">
            <CreateRateForm cabintoEdit={order} />
          </Modal.Window>

          <Modal.Window name="delete">
            <ConfirmDelete
              resourceName="cabin"
              disabled={isLoading}
              onConfirm={() => deleteCabin(orderId)}
            />
          </Modal.Window>
        </Modal>
      </div>
    </Table.Row>
  );
}
