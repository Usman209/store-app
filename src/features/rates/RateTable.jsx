import Table from "../../ui/Table";
import Spinner from "../../ui/Spinner";
import RateRow from "./RateRow";
// import useRates from "../../services/useRates";
import useOrders from "../../services/useOrders";

import Menus from "../../ui/Menus";
import styles from "./SearchBar.module.css";
import { useState } from "react";
import useItems from "../../services/useItems";

// const TableHeader = styled.header`
//   display: grid;
//   grid-template-columns: 0.6fr 1.8fr 2.2fr 1fr 1fr 1fr;
//   column-gap: 2.4rem;
//   align-items: center;

//   background-color: var(--color-grey-50);
//   border-bottom: 1px solid var(--color-grey-100);
//   text-transform: uppercase;
//   letter-spacing: 0.4px;
//   font-weight: 600;
//   color: var(--color-grey-600);
//   padding: 1.6rem 2.4rem;
// `;

function RateTable() {
  const [value, setValue] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  //  const { isLoading, error, orders } = useRates();
  const { isLoading, error, orders } = useItems();

  if (error) return <div>Error: {error}</div>;

  if (isLoading) return <Spinner />;

  // Assuming 'dataArray' is your original array of objects

  const newArray = orders.map((item) => {
    return {
      id: item._id,
      name: item.name,
      description: item.description,
      priceCleaning: item.priceCleaning,
      priceCleaningWithDeduction: item.priceCleaningWithDeduction,
      priceCleaningWithDeductionFactor: item.priceCleaningWithDeductionFactor,
      priceGriding: item.priceGriding,
      priceGridingWithDeduction: item.priceGridingWithDeduction,
      priceGridingWithDeductionFactor: item.priceGridingWithDeductionFactor,
    };
  });

  console.log("===============>", newArray);

  // Filter orders based on the search value
  //  const filteredOrders = orders.filter(
  //    (order) => order?.name?.toLowerCase().includes(value.toLowerCase()) // Assuming 'name' is the property to search
  //  );

  return (
    <Menus>
      <div className={styles.container}>
        <input
          type="text"
          className={styles.textbox}
          placeholder="Search data..."
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
          }}
        />
      </div>
      <Table columns="0.6fr 1.8fr 2.2fr 1fr 1fr 1fr">
        <Table.Header>
          <div>Name</div>
          <div>Cleaning</div>
          <div>Cleaning/D</div>
          <div>Cleaning/DF</div>
          <div>Griding</div>
          <div>Griding/D</div>
          <div>Griding/DF</div>
        </Table.Header>
        <Table.Body
          data={newArray} // Render filtered orders
          render={(order) => <RateRow order={order} key={order.id} />}
        />
      </Table>
    </Menus>
  );
}
export default RateTable;
