import styled from "styled-components";

import Input from "../../ui/Input";
import Form from "../../ui/Form";
import Button from "../../ui/Button";
import FileInput from "../../ui/FileInput";
import Textarea from "../../ui/Textarea";
import { useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "@tanstack/react-query";
// import { createEditRate } from "../../services/apiRates";
import toast from "react-hot-toast";
import FormRow from "../../ui/FormRow";
import useCreateType from "../../services/useCreateType";
import { useEffect, useState } from "react";
import useSettings from "../settings/useSettings";
import useEditOrder from "../../services/useEditOrder";
import useEditType from "../../services/useEditType";

const Label = styled.label`
  font-pricecleaning: 500;
`;

const Error = styled.span`
  font-size: 1.4rem;
  color: var(--color-red-700);
`;

const StyledFormRow = styled.div`
  display: flex;
  align-items: center;
  grid-template-columns: repeat(2, auto); /* Adjust the number of columns */
  gap: 3rem; /* Adjust the gap between elements */
  /* Additional styles for your grid */
`;

const StyledTextLine = styled.span`
  color: #333333 100%;
  font-size: 17px;
  font-family: NeuropoliticalRg-Regular;
  word-spacing: 0px;
`;

function CreateRateForm({ cabintoEdit = {}, onCloseModal }) {
  const {
    isLoading,
    settings: {
      minBookingLength,
      maxBookingLength,
      maxGuestsPerBooking,
      breakfastPrice,
      flour,
      flourSafai,
      floorKhootPrice,
      flourKhootQunatity,
      gram,
      gramSafai,
      gramKhootPrice,
      gramKhootQunatity,
      turmericOrChilli,
      turmericOrChilliSafai,
      turmericOrChilliKhootPrice,
      turmericOrChilliKhootQunatity,
    } = {},
  } = useSettings();

  const [formData, setFormData] = useState({
    type: "flour", // Default type
    isCleanWithDeduction: false,
    isCleanWithoutDeduction: false,
    isGrindWithDeduction: false,
    isGrindWithoutDeduction: false,
    netWeight: "",
    finalPrice: 0,
    balance: 0, // New state for balance
    editablePrice: 0, // Initialize editablePrice
  });

  const {
    type,
    isCleanWithDeduction,
    isCleanWithoutDeduction,
    isGrindWithDeduction,
    isGrindWithoutDeduction,
    netWeight,
    finalPrice,
  } = formData;

  const [advance, setAdvance] = useState(0);
  const [initialWeight, setInitialWeight] = useState(0);

  const { id: editId, ...editValues } = cabintoEdit;

  const [isFlourSelected, setIsFlourSelected] = useState(true);
  const [isGramSelected, setIsGramSelected] = useState(false);
  const [isTurmericOrChilliSelected, setIsTurmericOrChilliSelected] =
    useState(false);
  const [isCleanSelected, setIsCleanSelected] = useState(false);
  const [isDeductionSelected, setIsDeductionSelected] = useState(false);
  const [rate, setRate] = useState(0);

  const [balanceItem, setBalanceItem] = useState(0);

  const [items, setItems] = useState([]);
  let [selectedItem, setSelectedItem] = useState(null);

  const [item, setItem] = useState();

  // const [finalPrice, setFinalPrice] = useState(0); // Initialize with a default value
  console.log("val=================", selectedItem);
  console.log("item=================", item);

  const isEditSession = Boolean(editId);

  const {
    register,
    handleSubmit,
    reset,
    getValues,
    formState,
    watch, // Include watch from react-hook-form
  } = useForm({
    defaultValues: isEditSession ? editValues : {},
  });
  const { errors } = formState;
  const queryClient = useQueryClient();

  // const { isCreating, createRate } = useCreateRate();
  const { isCreating, createType } = useCreateType();
  // const { isEditing, editRate } = useEditRate();
  const { isEditing, editOrder } = useEditType();

  const watchedWeight = watch("priceCleaning");
  const watchedAdvance = watch("advance");
  // const watchedCustomDeduction = watch("customDeduction");

  const isWorking = isCreating || isEditing;

  const calculateFinalPrice = (
    data,
    isSelected,
    rate,
    clean,
    deduction,
    safai,
    khootPrice,
    quantity
  ) => {
    const priceCleaning = data.priceCleaning || 0;
    const advance = data.advance || 0;
    let rateValue = rate;
    let calculatedPrice = 0;
    let balance = priceCleaning;

    // {PriceCleannig,priceCleaningWithDeduction,priceCleaningWithDeductionFactor,priceGriding,priceGridingWithDeduction,priceGridingWithDeductionFactor}

    if (isSelected) {
      setBalanceItem(balance);

      if (deduction) {
        balance = priceCleaning - quantity;
        setBalanceItem(balance);
      }

      if (deduction && !clean) {
        rateValue = khootPrice;
      } else if (clean && deduction) {
        rateValue = khootPrice + safai;
      } else if (clean && !deduction) {
        rateValue = rate + safai;
      }

      if (advance > 0) {
        const newBalance = balance - advance;
        setBalanceItem(newBalance);
      }

      calculatedPrice = !deduction
        ? rateValue * priceCleaning
        : priceCleaning * rateValue;
    }

    setRate(rateValue);
  };

  useEffect(() => {
    // Define the cleaning and grinding functions
    const cleaning = (
      isCleanWithDeduction,
      isCleanWithoutDeduction,
      priceCleaning
    ) => {
      if (isCleanWithoutDeduction) {
        return 8 * priceCleaning;
      } else if (isCleanWithDeduction) {
        const priceCleaningSubtract = priceCleaning * 0.025;
        const res = priceCleaning - priceCleaningSubtract;
        return res * 4;
      }
      return 0;
    };

    const grinding = (
      isGrindWithDeduction,
      isGrindWithoutDeduction,
      priceCleaning
    ) => {
      if (isGrindWithDeduction) {
        const deduction = 0.025 * priceCleaning;
        const newWeight = priceCleaning - deduction; // Update priceCleaning after deduction
        return newWeight * 7;
      } else if (isGrindWithoutDeduction) {
        return 12 * priceCleaning;
      }
      return 0;
    };

    const fetchItems = async () => {
      try {
        const response = await fetch("http://localhost:4000/api/products");
        if (!response.ok) {
          throw new Error("Failed to fetch items");
        }
        const data = await response.json();
        setItems(data);
        setItem(data[0]);
      } catch (error) {
        console.error(error);
      }
    };

    fetchItems();

    const fetchItem = async () => {
      try {
        const response = await fetch(
          `http://localhost:4000/api/products/${selectedItem}`
        );
        if (!response.ok) {
          throw new Error("Failed to fetch items");
        }
        const data = await response.json();
        setItem(data);
      } catch (error) {
        console.error(error);
      }
    };

    if (selectedItem) {
      fetchItem();
    }

    // Calculate the prices for cleaning and grinding
    const cleaningPrice = cleaning(
      isCleanWithDeduction,
      isCleanWithoutDeduction,
      watchedWeight
    );
    const grindingPrice = grinding(
      isGrindWithDeduction,
      isGrindWithoutDeduction,
      watchedWeight
    );
    const rate = cleaningPrice + grindingPrice;

    // Calculate the new balance
    let newWeight = watchedWeight;
    if (isGrindWithDeduction) {
      const deduction = Math.round(0.025 * newWeight); // Round the deduction to the nearest integer
      newWeight -= deduction; // Update priceCleaning after deduction
    }
    if (isCleanWithDeduction) {
      const deduction = Math.round(0.025 * newWeight); // Round the deduction to the nearest integer

      newWeight -= deduction; // Update priceCleaning after deduction
    } else if (isCleanWithoutDeduction) {
      const deduction = 0.0; // No deduction
      newWeight -= deduction; // Update priceCleaning
    }

    const newBalance = newWeight - advance;

    // Set the form data with updated values
    setFormData((prevData) => ({
      ...prevData,
      finalPrice: rate,
      editablePrice: rate, // Initialize editablePrice with the calculated value
      balance: newBalance,
    }));
    // Update the initial priceCleaning if it's not set
    if (!initialWeight) {
      setInitialWeight(watchedWeight);
    }
  }, [
    isCleanWithDeduction,
    isCleanWithoutDeduction,
    isGrindWithDeduction,
    isGrindWithoutDeduction,
    watchedWeight,
    advance,
    selectedItem,
  ]);

  function onSubmit(data) {
    console.log(data);

    if (isFlourSelected) {
      calculateFinalPrice(
        data,
        isFlourSelected,
        flour,
        isCleanSelected,
        isDeductionSelected,
        flourSafai,
        floorKhootPrice,
        flourKhootQunatity
      );
    }

    if (isGramSelected) {
      calculateFinalPrice(
        data,
        isGramSelected,
        gram,
        isCleanSelected,
        isDeductionSelected,
        gramSafai,
        gramKhootPrice
      );
    }

    if (isTurmericOrChilliSelected) {
      calculateFinalPrice(
        data,
        isTurmericOrChilliSelected,
        turmericOrChilli,
        isCleanSelected,
        isDeductionSelected,
        turmericOrChilliSafai,
        turmericOrChilliKhootPrice
      );
    }

    data.isClean = isCleanSelected;
    data.isDeduction = isDeductionSelected;

    const selectedOption = {
      isFlourSelected: isFlourSelected,
      isGramSelected: isGramSelected,
      isTurmericOrChilliSelected: isTurmericOrChilliSelected,
    };

    const selectedKey = Object.keys(selectedOption).find(
      (key) => selectedOption[key] === true
    );

    const keyToString = {
      isFlourSelected: "flour",
      isGramSelected: "gram",
      isTurmericOrChilliSelected: "turmericOrChilli",
    };

    if (selectedKey) {
      data.type = keyToString[selectedKey];
    }

    // data.price = finalPrice;
    data.price = formData.editablePrice.toFixed(2);
    // Rest of your logic here
    // ...

    console.log("data is :", data);

    // data.balanceItem = String(20);
    // data.advance = String( 4 );

    // if ("created_at" in data) {
    //   delete data.created_at;
    // }
    // { newRateData: { ...data }, id: editId },

    if (isEditSession) {
      console.log("Data to be edited: ", data);
      console.log("Edit ID: ", editId);
      editOrder(
        {
          newOrderData: data, // Pass an empty string for the image
          id: editId, // Your ID value here
        },
        {
          onSuccess: (data) => {
            reset();
            onCloseModal?.();
          },
        }
      );
    } else
      createType(
        { ...data },
        {
          onSuccess: (data) => {
            reset();
            onCloseModal?.();
          },
        }
      );
  }

  function onError(error) {
    console.log(error);
  }

  return (
    <Form
      onSubmit={handleSubmit(onSubmit, onError)}
      type={onCloseModal ? "modal" : "regular"}
    >
      <FormRow label="Item Type" error={errors?.name?.message}>
        <Input
          type="text"
          id="name"
          disabled={isWorking}
          {...register("name", {
            required: "this field required",
          })}
        />
      </FormRow>

      {/* <FormRow label="Customer mobile" error={errors?.customerMobile?.message}>
        <Input
          type="text"
          id="customerMobile"
          disabled={isWorking}
          {...register("customerMobile", {
            required: "this field required",
          })}
        />
      </FormRow> */}

      <FormRow label="priceCleaning">
        <Input
          type="number"
          id="priceCleaning"
          disabled={isWorking}
          {...register("priceCleaning", { required: "This field is required" })}
        />
      </FormRow>

      <FormRow label="priceCleaningWithDeduction">
        <Input
          type="number"
          id="priceCleaningWithDeduction"
          disabled={isWorking}
          {...register("priceCleaningWithDeduction", {
            required: "This field is required",
          })}
        />
      </FormRow>

      <FormRow label="priceCleaningWithDeductionFactor">
        <Input
          type="number"
          id="priceCleaningWithDeductionFactor"
          disabled={isWorking}
          {...register("priceCleaningWithDeductionFactor", {
            required: "This field is required",
          })}
        />
      </FormRow>

      <FormRow label="priceGriding">
        <Input
          type="number"
          id="priceGriding"
          disabled={isWorking}
          {...register("priceGriding", {
            required: "This field is required",
          })}
        />
      </FormRow>

      <FormRow label="priceGridingWithDeduction">
        <Input
          type="number"
          id="priceGridingWithDeduction"
          disabled={isWorking}
          {...register("priceGridingWithDeduction", {
            required: "This field is required",
          })}
        />
      </FormRow>

      <FormRow label="priceGridingWithDeductionFactor">
        <Input
          type="number"
          id="priceGridingWithDeductionFactor"
          disabled={isWorking}
          {...register("priceGridingWithDeductionFactor", {
            required: "This field is required",
          })}
        />
      </FormRow>

      <FormRow>
        {/* type is an HTML attribute! */}
        <Button
          variation="secondary"
          type="reset"
          onClick={() => onCloseModal?.()}
        >
          Cancel
        </Button>
        <Button disabled={isWorking}>
          {isEditSession ? "Edit type" : "Add type"}
        </Button>
      </FormRow>
    </Form>
  );
}

export default CreateRateForm;
