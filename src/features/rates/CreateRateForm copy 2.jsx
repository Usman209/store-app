import styled from "styled-components";

import Input from "../../ui/Input";
import Form from "../../ui/Form";
import Button from "../../ui/Button";
import FileInput from "../../ui/FileInput";
import Textarea from "../../ui/Textarea";
import { useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createEditRate } from "../../services/apiRates";
import toast from "react-hot-toast";
import FormRow from "../../ui/FormRow";
import useCreateRate from "../../services/useCreateRate";
import useEditRate from "../../services/useEditRate";
import { useEffect, useState } from "react";
import useSettings from "../settings/useSettings";

const Label = styled.label`
  font-weight: 500;
`;

const Error = styled.span`
  font-size: 1.4rem;
  color: var(--color-red-700);
`;

const StyledFormRow = styled.div`
  display: flex;
  align-items: center;
  grid-template-columns: repeat(2, auto); /* Adjust the number of columns */
  gap: 3rem; /* Adjust the gap between elements */
  /* Additional styles for your grid */
`;

const StyledTextLine = styled.span`
  color: #333333 100%;
  font-size: 17px;
  font-family: NeuropoliticalRg-Regular;
  word-spacing: 0px;
`;

function CreateRateForm({ cabintoEdit = {}, onCloseModal }) {
  const {
    isLoading,
    settings: {
      minBookingLength,
      maxBookingLength,
      maxGuestsPerBooking,
      breakfastPrice,
      flour,
      flourSafai,
      floorKhootPrice,
      flourKhootQunatity,
      gram,
      gramSafai,
      gramKhootPrice,
      gramKhootQunatity,
      turmericOrChilli,
      turmericOrChilliSafai,
      turmericOrChilliKhootPrice,
      turmericOrChilliKhootQunatity,
    } = {},
  } = useSettings();

  const { id: editId, ...editValues } = cabintoEdit;

  const [isFlourSelected, setIsFlourSelected] = useState(true);
  const [isGramSelected, setIsGramSelected] = useState(false);
  const [isTurmericOrChilliSelected, setIsTurmericOrChilliSelected] =
    useState(false);
  const [isCleanSelected, setIsCleanSelected] = useState(false);
  const [isDeductionSelected, setIsDeductionSelected] = useState(false);
  const [rate, setRate] = useState(0);

  const [finalPrice, setFinalPrice] = useState(0); // Initialize with a default value

  const selectedOptions = {
    flour: isFlourSelected,
    gram: isGramSelected,
    "turmeric-chilli": isTurmericOrChilliSelected,
    clean: isCleanSelected,
    deduction: isDeductionSelected,
  };

  const isEditSession = Boolean(editId);

  const handleOptionChange = (e) => {
    const { id, checked } = e.target;

    switch (id) {
      case "flour":
        setIsFlourSelected(checked);
        break;
      case "gram":
        setIsGramSelected(checked);
        break;
      case "turmeric-chilli":
        setIsTurmericOrChilliSelected(checked);
        break;
      case "clean":
        setIsCleanSelected(checked);
        break;
      case "deduction":
        setIsDeductionSelected(checked);
        break;
      default:
        break;
    }
  };

  const {
    register,
    handleSubmit,
    reset,
    getValues,
    formState,
    watch, // Include watch from react-hook-form
  } = useForm({
    defaultValues: isEditSession ? editValues : {},
  });
  const { errors } = formState;
  const queryClient = useQueryClient();

  const { isCreating, createRate } = useCreateRate();
  const { isEditing, editRate } = useEditRate();

  const watchedWeight = watch("weight");

  const isWorking = isCreating || isEditing;

  const calculateFinalPrice = (data) => {
    let calculatedPrice = 0;
    let rateValue = 0;

    const weight = data.weight || 0; // Ensure weight has a default value if not provided

    if (isFlourSelected) {
      rateValue = flour; // Set a default rate if no other conditions are met
      setRate(rateValue);

      if (isDeductionSelected & !isCleanSelected) {
        rateValue = floorKhootPrice;
        setRate(rateValue);
      }
      if (isCleanSelected && isDeductionSelected) {
        rateValue = floorKhootPrice + flourSafai;
        setRate(rateValue);
      } else if (isCleanSelected && !isDeductionSelected) {
        rateValue = flour + flourSafai;
        setRate(rateValue);
      }

      if (!isDeductionSelected) {
        calculatedPrice = rateValue * weight;
      } else {
        calculatedPrice = weight * rateValue;
      }
    }

    setFinalPrice(calculatedPrice);
  };

  useEffect(() => {
    calculateFinalPrice(getValues());
  }, [isFlourSelected, isDeductionSelected, isCleanSelected, watchedWeight]);

  function onSubmit(data) {
    calculateFinalPrice(data);

    console.log("data is :", data);
    // const image = typeof data.image === "string" ? data.image : data.image[0];
    // if (isEditSession) {
    //   editRate(
    //     { newRateData: { ...data, image }, id: editId },
    //     {
    //       onSuccess: (data) => {
    //         reset();
    //         onCloseModal?.();
    //       },
    //     }
    //   );
    // } else
    //   createRate(
    //     { ...data, image: image },
    //     {
    //       onSuccess: (data) => {
    //         reset();
    //         onCloseModal?.();
    //       },
    //     }
    //   );
  }

  function onError(error) {
    // console.log(error );
  }

  return (
    <Form
      onSubmit={handleSubmit(onSubmit, onError)}
      type={onCloseModal ? "modal" : "regular"}
    >
      <FormRow label="Type">
        <StyledFormRow>
          <div style={{ display: "flex" }}>
            <input
              type="checkbox"
              id="flour"
              checked={isFlourSelected}
              onChange={() => {
                setIsFlourSelected(true);
                setIsGramSelected(false);
                setIsTurmericOrChilliSelected(false);
              }}
              disabled={isWorking}
              style={{ marginRight: "5px" }}
            />
            <Label htmlFor="flour" style={{ fontSize: "16px" }}>
              Flour
            </Label>
          </div>
          <div style={{ display: "flex" }}>
            <input
              type="checkbox"
              id="gram"
              checked={isGramSelected}
              onChange={() => {
                setIsGramSelected(true);
                setIsFlourSelected(false);
                setIsTurmericOrChilliSelected(false);
              }}
              disabled={isWorking}
              style={{ marginRight: "5px" }}
            />
            <Label htmlFor="gram" style={{ fontSize: "16px" }}>
              Gram
            </Label>
          </div>
          <div style={{ display: "flex" }}>
            <input
              type="checkbox"
              id="turmeric-chilli"
              checked={isTurmericOrChilliSelected}
              onChange={() => {
                setIsTurmericOrChilliSelected(true);
                setIsFlourSelected(false);
                setIsGramSelected(false);
              }}
              disabled={isWorking}
              style={{ marginRight: "5px" }}
            />
            <Label htmlFor="turmeric-chilli" style={{ fontSize: "16px" }}>
              Turmeric/Chilli
            </Label>
          </div>
        </StyledFormRow>
      </FormRow>

      <FormRow label="Select" style={{ flexBasis: "40%" }}>
        <div>
          <input
            type="checkbox"
            id="clean"
            checked={isCleanSelected}
            onChange={handleOptionChange}
            disabled={isWorking}
          />
          <Label
            htmlFor="clean"
            style={{ fontSize: "16px", marginBottom: "5px" }}
          >
            Clean/Safai
          </Label>
        </div>
        <div>
          <input
            type="checkbox"
            id="deduction"
            checked={isDeductionSelected}
            onChange={handleOptionChange}
            disabled={isWorking}
          />
          <Label
            htmlFor="deduction"
            style={{ fontSize: "16px", marginBottom: "5px" }}
          >
            Deduction/Khoot
          </Label>
        </div>
      </FormRow>

      <FormRow label="Customer name" error={errors?.name?.message}>
        <Input
          type="text"
          id="name"
          disabled={isWorking}
          {...register("name", {
            required: "this field required",
          })}
        />
      </FormRow>

      <FormRow label="Customer mobile" error={errors?.mobile?.message}>
        <Input
          type="text"
          id="mobile"
          disabled={isWorking}
          {...register("mobile", {
            required: "this field required",
          })}
        />
      </FormRow>

      <FormRow label="Weight" error={errors?.weight?.message}>
        <Input
          type="number"
          id="weight"
          disabled={isWorking}
          {...register("weight", {
            required: "this field required",
          })}
        />
      </FormRow>

      <FormRow label="Rate" error={errors?.rate?.message}>
        <Input
          type="number"
          id="rate"
          value={rate} // Use 'value' instead of 'defaultValue'
          disabled={isWorking}
          {...register("rate", {
            required: "this field required",
          })}
        />
      </FormRow>

      <FormRow label="Balance Item" error={errors?.balanceItem?.message}>
        <Input
          type="text"
          id="balanceItem"
          disabled={isWorking}
          {...register("balanceItem", {
            required: "this field required",
          })}
        />
      </FormRow>

      <FormRow label="Discount" error={errors?.discount?.message}>
        <Input
          type="number"
          id="discount"
          disabled={isWorking}
          defaultValue={0}
          // {...register("discount", {
          //   required: "this field required",
          //   validate: (value) =>
          //     value <= getValues().regularPrice ||
          //     "discount should be less than regualr price",
          // })}
        />
      </FormRow>

      <FormRow label="Price" error={errors?.regularPrice?.message}>
        <StyledTextLine>{finalPrice}</StyledTextLine>
      </FormRow>

      {/* <FormRow label="Price" error={errors?.regularPrice?.message}>
        <Input
          type="number"
          id="regularPrice"
          disabled={isWorking}
          {...register("regularPrice", {
            required: "this field required",
            min: {
              value: 1,
              message: "message should be at least 1",
            },
          })}
        />
      </FormRow> */}

      <FormRow label="Advance" error={errors?.advance?.message}>
        <Input
          type="number"
          id="advance"
          disabled={isWorking}
          {...register("advance", {
            required: "this field required",
            min: {
              value: 1,
              message: "message should be at least 1",
            },
          })}
        />
      </FormRow>

      <FormRow
        label="Custom deduction"
        error={errors?.customDeduction?.message}
      >
        <Input
          type="number"
          id="customDeduction"
          disabled={isWorking}
          {...register("customDeduction", {
            required: "this field required",
            min: {
              value: 1,
              message: "message should be at least 1",
            },
          })}
        />
      </FormRow>

      <FormRow label="comment" error={errors?.comment?.message}>
        <Textarea
          type="number"
          id="comment"
          disabled={isWorking}
          defaultValue=""
          {...register("comment", {
            required: "this field required",
          })}
        />
      </FormRow>

      {/* <FormRow label="Rate photo">
        <FileInput
          id="image"
          disabled={isWorking}
          accept="image/*"
          {...register("image", {
            required: isEditSession ? false : "this field required",
          })}
        />
      </FormRow> */}

      <FormRow>
        {/* type is an HTML attribute! */}
        <Button
          variation="secondary"
          type="reset"
          onClick={() => onCloseModal?.()}
        >
          Cancel
        </Button>
        <Button disabled={isWorking}>
          {isEditSession ? "Edit cabin" : "Add cabin"}
        </Button>
      </FormRow>
    </Form>
  );
}

export default CreateRateForm;
