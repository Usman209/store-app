import styled from "styled-components";

import Input from "../../ui/Input";
import Form from "../../ui/Form";
import Button from "../../ui/Button";
import FileInput from "../../ui/FileInput";
import Textarea from "../../ui/Textarea";
import { useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createEditRate } from "../../services/apiRates";
import toast from "react-hot-toast";
import FormRow from "../../ui/FormRow";
import useCreateRate from "../../services/useCreateRate";
import useEditRate from "../../services/useEditRate";
import { useEffect, useState } from "react";
import useSettings from "../settings/useSettings";
import useEditOrder from "../../services/useEditOrder";

import useCreateOrder from "../../services/useCreateOrder";

const Label = styled.label`
  font-weight: 500;
`;

const Error = styled.span`
  font-size: 1.4rem;
  color: var(--color-red-700);
`;

const StyledFormRow = styled.div`
  display: flex;
  align-items: center;
  grid-template-columns: repeat(2, auto); /* Adjust the number of columns */
  gap: 3rem; /* Adjust the gap between elements */
  /* Additional styles for your grid */
`;

const StyledTextLine = styled.span`
  color: #333333 100%;
  font-size: 17px;
  font-family: NeuropoliticalRg-Regular;
  word-spacing: 0px;
`;

function CreateRateForm({ cabintoEdit = {}, onCloseModal }) {
  const [formData, setFormData] = useState({
    type: "flour", // Default type
    isCleanWithDeduction: false,
    isCleanWithoutDeduction: false,
    isGrindWithDeduction: false,
    isGrindWithoutDeduction: false,
    netWeight: "",
    finalPrice: 0,
    balance: 0, // New state for balance
  });

  const {
    type,
    isCleanWithDeduction,
    isCleanWithoutDeduction,
    isGrindWithDeduction,
    isGrindWithoutDeduction,
    netWeight,
    finalPrice,
  } = formData;

  const [advance, setAdvance] = useState(0);
  const [initialWeight, setInitialWeight] = useState(0);

  const { register, handleSubmit, reset, watch } = useForm({
    defaultValues: cabintoEdit,
  });

  console.log("type================", type);

  const isEditSession = Boolean(cabintoEdit.id);
  const isWorking = useCreateOrder.isCreating || useEditOrder.isEditing;

  const watchedWeight = watch("weight");

  useEffect(() => {
    // Define the cleaning and grinding functions
    const cleaning = (
      isCleanWithDeduction,
      isCleanWithoutDeduction,
      weight
    ) => {
      if (isCleanWithoutDeduction) {
        return 8 * weight;
      } else if (isCleanWithDeduction) {
        const weightSubtract = weight * 0.025;
        const res = weight - weightSubtract;
        return res * 4;
      }
      return 0;
    };

    const grinding = (
      isGrindWithDeduction,
      isGrindWithoutDeduction,
      weight
    ) => {
      if (isGrindWithDeduction) {
        const deduction = 0.025 * weight;
        const newWeight = weight - deduction; // Update weight after deduction
        return newWeight * 8;
      } else if (isGrindWithoutDeduction) {
        return 12 * weight;
      }
      return 0;
    };

    // Calculate the prices for cleaning and grinding
    const cleaningPrice = cleaning(
      isCleanWithDeduction,
      isCleanWithoutDeduction,
      watchedWeight
    );
    const grindingPrice = grinding(
      isGrindWithDeduction,
      isGrindWithoutDeduction,
      watchedWeight
    );
    const rate = cleaningPrice + grindingPrice;

    // Calculate the new balance
    let newWeight = watchedWeight;
    if (isGrindWithDeduction) {
      const deduction = Math.round(0.025 * newWeight); // Round the deduction to the nearest integer
      newWeight -= deduction; // Update weight after deduction
    }
    if (isCleanWithDeduction) {
      const deduction = Math.round(0.025 * newWeight); // Round the deduction to the nearest integer

      newWeight -= deduction; // Update weight after deduction
    } else if (isCleanWithoutDeduction) {
      const deduction = 0.0; // No deduction
      newWeight -= deduction; // Update weight
    }

    const newBalance = newWeight - advance;

    // Set the form data with updated values
    setFormData({ ...formData, finalPrice: rate, balance: newBalance });

    // Update the initial weight if it's not set
    if (!initialWeight) {
      setInitialWeight(watchedWeight);
    }
  }, [
    isCleanWithDeduction,
    isCleanWithoutDeduction,
    isGrindWithDeduction,
    isGrindWithoutDeduction,
    watchedWeight,
    advance,
  ]);

  const onSubmit = (data) => {
    if (isEditSession) {
      useEditOrder.editOrder(
        { newOrderData: { ...data }, id: cabintoEdit.id },
        {
          onSuccess: () => {
            reset();
            onCloseModal?.();
          },
        }
      );
    } else {
      useCreateOrder.createOrder(data, {
        onSuccess: () => {
          reset();
          onCloseModal?.();
        },
      });
    }
  };

  const onError = (error) => {
    // Error handling logic here
    console.log("error -------------", error);
  };

  return (
    <Form
      onSubmit={handleSubmit(onSubmit, onError)}
      type={onCloseModal ? "modal" : "regular"}
    >
      <FormRow label="Type">
        <select
          id="type"
          value={formData.type}
          onChange={(e) => setFormData({ ...formData, type: e.target.value })}
          disabled={isWorking}
        >
          <option value="flour">Flour</option>
          <option value="grams">Grams</option>
          {/* Add more options as needed */}
        </select>
      </FormRow>

      <FormRow label="Select Clean">
        <div>
          <input
            type="checkbox"
            id="clean-with-deduction"
            checked={isCleanWithDeduction}
            onChange={(e) => {
              setFormData({
                ...formData,
                isCleanWithDeduction: e.target.checked,
                isCleanWithoutDeduction: !e.target.checked
                  ? isCleanWithoutDeduction
                  : false,
              });
            }}
            disabled={isWorking}
          />
          <Label htmlFor="clean-with-deduction">Cleaning with Deduction</Label>
        </div>
        <div>
          <input
            type="checkbox"
            id="clean-without-deduction"
            checked={isCleanWithoutDeduction}
            onChange={(e) => {
              setFormData({
                ...formData,
                isCleanWithoutDeduction: e.target.checked,
                isCleanWithDeduction: !e.target.checked
                  ? isCleanWithDeduction
                  : false,
              });
            }}
            disabled={isWorking}
          />
          <Label htmlFor="clean-without-deduction">
            Cleaning without Deduction
          </Label>
        </div>
      </FormRow>

      <FormRow label="Select Grind">
        <div>
          <input
            type="checkbox"
            id="grinding-with-deduction"
            checked={isGrindWithDeduction}
            onChange={(e) => {
              setFormData({
                ...formData,
                isGrindWithDeduction: e.target.checked,
                isGrindWithoutDeduction: !e.target.checked
                  ? isGrindWithoutDeduction
                  : false,
              });
            }}
            disabled={isWorking}
          />
          <Label htmlFor="grinding-with-deduction">
            Grinding with Deduction
          </Label>
        </div>
        <div>
          <input
            type="checkbox"
            id="grinding-without-deduction"
            checked={isGrindWithoutDeduction}
            onChange={(e) => {
              setFormData({
                ...formData,
                isGrindWithoutDeduction: e.target.checked,
                isGrindWithDeduction: !e.target.checked
                  ? isGrindWithDeduction
                  : false,
              });
            }}
            disabled={isWorking}
          />
          <Label htmlFor="grinding-without-deduction">
            Grinding without Deduction
          </Label>
        </div>
      </FormRow>

      <FormRow label="Weight">
        <Input
          type="number"
          id="weight"
          disabled={isWorking}
          {...register("weight", { required: "This field is required" })}
        />
      </FormRow>

      <FormRow label="Rate">
        <Input
          type="number"
          id="rate"
          value={finalPrice}
          disabled={isWorking}
          readOnly
        />
      </FormRow>

      <FormRow label="Balance">
        <Input
          type="text"
          id="balance"
          value={formData?.balance} // Display balance with 2 decimal places
          disabled
        />
      </FormRow>

      <FormRow label="Advance">
        <Input
          type="number"
          id="advance"
          disabled={isWorking}
          value={advance}
          onChange={(e) => setAdvance(parseFloat(e.target.value))}
        />
      </FormRow>

      <FormRow label="Comment">
        <Textarea id="comment" disabled={isWorking} {...register("comment")} />
      </FormRow>

      <FormRow>
        <Button
          variation="secondary"
          type="reset"
          onClick={() => onCloseModal?.()}
        >
          Cancel
        </Button>
        <Button disabled={isWorking}>
          {isEditSession ? "Edit order" : "Add order"}
        </Button>
      </FormRow>
    </Form>
  );
}

export default CreateRateForm;
